package com.seminario.backend.repository;

import com.seminario.backend.model.Inability;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InabilityRepository extends JpaRepository<Inability, Long> {

    Inability getByIncapId(Long id);
}
