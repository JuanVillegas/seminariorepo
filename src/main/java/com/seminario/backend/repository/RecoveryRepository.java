package com.seminario.backend.repository;

import com.seminario.backend.model.Recovery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RecoveryRepository extends JpaRepository<Recovery, Long> {

    @Query(nativeQuery = true, value = "SELECT " +
            "reco_id, " +
            "reco_descripcion, " +
            "fk_incapacidad_id, " +
            "fk_trabajador_id, " +
            "fk_entidad_id " +
            "FROM semi_reco.recobro "+
            "WHERE fk_trabajador_id =:idRecoveryEmployee")
    Recovery findByRecoveryEmplo(@Param(value = "idRecoveryEmployee")Long idRecoveryEmployee);

    @Query(nativeQuery = true, value = "SELECT " +
            "reco_id, " +
            "reco_descripcion, " +
            "fk_incapacidad_id, " +
            "fk_trabajador_id, " +
            "fk_entidad_id " +
            "FROM semi_reco.recobro "+
            "WHERE fk_entidad_id =:idRecoveryEntity")
    Recovery findByRecoveryEntity(@Param(value = "idRecoveryEntity")Long idRecoveryEntity);

    @Query(nativeQuery = true, value = "SELECT " +
            "reco_id, " +
            "reco_descripcion, " +
            "fk_incapacidad_id, " +
            "fk_trabajador_id, " +
            "fk_entidad_id " +
            "FROM semi_reco.recobro "+
            "WHERE fk_incapacidad_id =:idRecoveryInability")
    Recovery findByRecoveryInability(@Param(value = "idRecoveryInability")Long idRecoveryInability);
}
