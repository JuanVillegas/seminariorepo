package com.seminario.backend.repository;

import com.seminario.backend.model.EpsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EpsEntityRepository extends JpaRepository<EpsEntity, Long> {

    EpsEntity getByEntId(Long id);
}
