package com.seminario.backend.repository;

import com.seminario.backend.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    @Query(nativeQuery = true, value = "SELECT " +
            "tb_id, " +
            "tb_nombre, " +
            "tb_apellido, " +
            "tb_edad, " +
            "tb_salario, " +
            "fk_entidad_id, "+
            "fk_incapacidad_id, " +
            "fk_empresa_id " +
            "FROM trabajador " +
            "WHERE fk_entidad_id =:idEntity")
    Employee findByEpsEntity(@Param(value = "idEntity") Long idEntity);

    @Query(nativeQuery = true, value = "SELECT " +
            "tb_id, " +
            "tb_nombre, " +
            "tb_apellido, " +
            "tb_edad, " +
            "tb_salario, " +
            "fk_entidad_id, "+
            "fk_incapacidad_id, " +
            "fk_empresa_id " +
            "FROM trabajador " +
            "WHERE tb_id =:idEmployee")
    Employee findByEmployee(@Param(value = "idEmployee") Long idEmployee);

    Employee getByTbId(Long id);
}
