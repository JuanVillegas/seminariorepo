package com.seminario.backend.controller;

import com.seminario.backend.model.Company;
import com.seminario.backend.model.request.CompanyRequest;
import com.seminario.backend.service.CompanyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/company")
@Api(tags = "Company")
public class CompanyController {

    private static final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private CompanyService companyService;

    @PostMapping(path = "/insert")
    @ApiOperation(value = "Insert Company", response = Company.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Company insertPainStory(@RequestBody CompanyRequest companyRequest) {
        Company company = modelMapper.map(companyRequest, Company.class);
        return companyService.saveCompany(company);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Company", response = Company.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Company updateCompany(@RequestBody CompanyRequest companyRequest) {
        Company company = modelMapper.map(companyRequest, Company.class);
        return companyService.updateCompany(company);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get All Company", response = Company.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public List<Company> getAllCompany() { return companyService.findAllCompany(); }

    @DeleteMapping(path = "/delete/id")
    @ApiOperation(value = "Delete Company By Id", response = Company.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public void deleteCompany(@RequestParam(name = "id") Long id) {
        companyService.deleteCompany(id);
    }
}