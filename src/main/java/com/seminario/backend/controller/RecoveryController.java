package com.seminario.backend.controller;

import com.seminario.backend.model.Inability;
import com.seminario.backend.model.Recovery;
import com.seminario.backend.model.request.RecoveryRequest;
import com.seminario.backend.service.RecoveryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/recovery")
@Api(tags = "Recovery")
public class    RecoveryController {

    private static final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private RecoveryService recoveryService;

    @PostMapping(path = "/insert")
    @ApiOperation(value = "Insert Recovery", response = Recovery.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Recovery insertRecovery(@RequestBody RecoveryRequest recoveryRequest) {
        Recovery recovery = modelMapper.map(recoveryRequest, Recovery.class);
        return recoveryService.saveRecovery(recovery);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Recovery", response = Recovery.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Recovery updateRecovery(@RequestBody RecoveryRequest recoveryRequest) {
        Recovery recovery = modelMapper.map(recoveryRequest, Recovery.class);
        return recoveryService.updateRecovery(recovery);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get All Recovery", response = Recovery.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public List<Recovery> getAllRecovery() { return recoveryService.findAllRecovery(); }

    @DeleteMapping(path = "/delete/id")
    @ApiOperation(value = "Delete Recovery By Id", response = Recovery.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public void deleteRecovery(@RequestParam(name = "id") Long id) { recoveryService.deleteRecovery(id); }

    @GetMapping(path = "/find/employee")
    @ApiOperation(value = "Find Recovery By Employee", response = Recovery.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Recovery findByRecoveryEmplo(@RequestParam(name = "idRecoveryEmployee") Long idRecoveryEmployee) {
        return recoveryService.findByRecoveryEmplo(idRecoveryEmployee);
    }

    @GetMapping(path = "/find/entity")
    @ApiOperation(value = "Find Recovery By Employee", response = Recovery.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Recovery findByRecoveryEntity(@RequestParam(name = "idRecoveryEntity") Long idRecoveryEntity) {
        return recoveryService.findByRecoveryEntity(idRecoveryEntity);
    }

    @GetMapping(path = "/find/inability")
    @ApiOperation(value = "Find Recovery By Inability", response = Inability.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Recovery findByRecoveryInability(@RequestParam(name = "idRecoveryInability") Long idRecoveryInability) {
        return recoveryService.findByRecoveryInability(idRecoveryInability);
    }
}
