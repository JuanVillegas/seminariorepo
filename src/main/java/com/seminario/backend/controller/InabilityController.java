package com.seminario.backend.controller;

import com.seminario.backend.model.Inability;
import com.seminario.backend.model.request.InabilityRequest;
import com.seminario.backend.service.InabilityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/inability")
@Api(tags = "Inability")
public class InabilityController {

    private static final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private InabilityService inabilityService;

    @PostMapping(path = "/insert")
    @ApiOperation(value = "Insert Inability", response = Inability.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Inability insertInability(@RequestBody InabilityRequest inabilityRequest) {
        Inability inability = modelMapper.map(inabilityRequest, Inability.class);
        return inabilityService.saveInability(inability);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Inability", response = Inability.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Inability updateInability(@RequestBody InabilityRequest inabilityRequest) {
        Inability inability = modelMapper.map(inabilityRequest, Inability.class);
        return inabilityService.updateInability(inability);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get All Inability", response = Inability.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public List<Inability> getAllInability() { return inabilityService.findAllInability(); }

    @DeleteMapping(path = "/delete/id")
    @ApiOperation(value = "Delete Inability By Id", response = Inability.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public void deleteInability(@RequestParam(name = "id") Long id) { inabilityService.deleteInability(id); }
}
