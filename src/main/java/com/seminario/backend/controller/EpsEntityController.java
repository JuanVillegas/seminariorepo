package com.seminario.backend.controller;

import com.seminario.backend.model.EpsEntity;
import com.seminario.backend.model.request.EpsEntityRequest;
import com.seminario.backend.service.EpsEntityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/eps/entity")
@Api(tags = "Eps Entity")
public class EpsEntityController {

    private static final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private EpsEntityService epsEntityService;

    @PostMapping(path = "/insert")
    @ApiOperation(value = "Insert Eps Entity", response = EpsEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public EpsEntity insertEpsEntity(@RequestBody EpsEntityRequest epsEntityRequest) {
        EpsEntity epsEntity = modelMapper.map(epsEntityRequest, EpsEntity.class);
        return epsEntityService.saveEpsEntity(epsEntity);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Eps Entity", response = EpsEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public EpsEntity updateEpsEntity(@RequestBody EpsEntityRequest epsEntityRequest) {
        EpsEntity epsEntity = modelMapper.map(epsEntityRequest, EpsEntity.class);
        return epsEntityService.updateEpsEntity(epsEntity);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get All Eps Entity", response = EpsEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public List<EpsEntity> getAllEpsEntity() { return epsEntityService.findAllEpsEntity(); }

    @DeleteMapping(path = "/delete/id")
    @ApiOperation(value = "Delete Eps Entity By Id", response = EpsEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public void deleteEpsEntity(@RequestParam(name = "id") Long id) { epsEntityService.deleteEpsEntity(id); }
}
