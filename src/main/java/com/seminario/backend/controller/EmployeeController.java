package com.seminario.backend.controller;

import com.seminario.backend.model.Employee;
import com.seminario.backend.model.request.EmployeeRequest;
import com.seminario.backend.service.EmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/employee")
@Api(tags = "Employee")
public class EmployeeController {

    private static final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private EmployeeService employeeService;

    @PostMapping(path = "/insert")
    @ApiOperation(value = "Insert Employee", response = Employee.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Employee insertEmployee(@RequestBody EmployeeRequest employeeRequest) {
        Employee employee = modelMapper.map(employeeRequest, Employee.class);
        return employeeService.saveEmployee(employee);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Employee", response = Employee.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Employee updateEmployee(@RequestBody EmployeeRequest employeeRequest) {
        Employee employee = modelMapper.map(employeeRequest, Employee.class);
        return employeeService.updateEmployee(employee);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get All Employee", response = Employee.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public List<Employee> getAllEmployee() { return employeeService.findAllEmployee(); }

    @DeleteMapping(path = "/delete/id")
    @ApiOperation(value = "Delete Employee By Id", response = Employee.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public void deleteEmployee(@RequestParam(name = "id") Long id) { employeeService.deleteEmployee(id); }

    @GetMapping(path = "/find/eps/entity")
    @ApiOperation(value = "Find Employee By Eps Entity", response = Employee.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Employee findByEpsEntity(@RequestParam(name = "idEntity") Long idEntity) {
        return employeeService.findByEpsEntity(idEntity);
    }

    @GetMapping(path = "/find/employee")
    @ApiOperation(value = "Find Employee By Eps Entity", response = Employee.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The table doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Employee findByEmployee(@RequestParam(name = "idEmployee") Long idEmployee) {
        return employeeService.findByEmployee(idEmployee);
    }
}
