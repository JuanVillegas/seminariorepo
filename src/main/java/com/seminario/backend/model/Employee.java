package com.seminario.backend.model;

import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;

@Entity
@Table(name = "trabajador")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tb_id")
    private Long tbId;

    @Column(name = "tb_nombre")
    private String nombre;

    @Column(name = "tb_apellido")
    private String apellido;

    @Column(name = "tb_edad")
    private Integer edad;

    @Column(name = "tb_salario")
    private Integer salario;

    @OneToOne
    @JoinColumn(name = "fk_entidad_id", referencedColumnName = "ent_id")
    private EpsEntity epsEntity;

    @OneToOne
    @JoinColumn(name = "fk_incapacidad_id", referencedColumnName = "incap_id")
    private Inability inability;

    @OneToOne
    @JoinColumn(name = "fk_empresa_id", referencedColumnName = "emp_id")
    private Company company;

    public Employee() {
        // Constructor
    }

    public Long getTbId() {
        return tbId;
    }

    public void setTbId(Long tbId) {
        this.tbId = tbId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public Integer getSalario() {
        return salario;
    }

    public void setSalario(Integer salario) {
        this.salario = salario;
    }

    public EpsEntity getEpsEntity() {
        return epsEntity;
    }

    public void setEpsEntity(EpsEntity epsEntity) {
        this.epsEntity = epsEntity;
    }

    public Inability getInability() {
        return inability;
    }

    public void setInability(Inability inability) {
        this.inability = inability;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}