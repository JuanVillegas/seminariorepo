package com.seminario.backend.model;

import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Table(name = "incapacidad")
public class Inability {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "incap_id")
    private Long incapId;

    @Column(name = "incap_tipo")
    private String tipo;

    @Column(name = "incap_descripcion")
    private String descripcion;

    @Column(name = "incap_auxilio")
    private Integer auxilio;

    public Inability() {
        // Constructor
    }

    public Long getIncapId() {
        return incapId;
    }

    public void setIncapId(Long incapId) {
        this.incapId = incapId;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getAuxilio() {
        return auxilio;
    }

    public void setAuxilio(Integer auxilio) {
        this.auxilio = auxilio;
    }
}