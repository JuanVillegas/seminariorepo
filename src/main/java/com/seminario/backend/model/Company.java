package com.seminario.backend.model;

import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;

@Entity
@Table(name = "empresa")
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "emp_id")
    private Long empId;

    @Column(name = "emp_nombre")
    private String nombre;

    @Column(name = "emp_direccion")
    private String direccion;

    @OneToOne
    @JoinColumn(name = "fk_entidad_id", referencedColumnName = "ent_id")
    private EpsEntity epsEntity;

    public Company() {
        // Constructor
    }

    public Long getEmpId() {
        return empId;
    }

    public void setEmpId(Long empId) {
        this.empId = empId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public EpsEntity getEpsEntity() {
        return epsEntity;
    }

    public void setEpsEntity(EpsEntity epsEntity) {
        this.epsEntity = epsEntity;
    }
}