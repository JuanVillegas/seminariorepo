package com.seminario.backend.model.request;

import com.seminario.backend.model.Employee;
import com.seminario.backend.model.EpsEntity;
import com.seminario.backend.model.Inability;

public class RecoveryRequest {

    private Long recoId;
    private String descripcion;
    private Inability inability;
    private Employee employee;
    private EpsEntity epsEntity;

    public RecoveryRequest() {
        // Constructor
    }

    public Long getRecoId() {
        return recoId;
    }

    public void setRecoId(Long recoId) {
        this.recoId = recoId;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Inability getInability() {
        return inability;
    }

    public void setInability(Inability inability) {
        this.inability = inability;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public EpsEntity getEpsEntity() {
        return epsEntity;
    }

    public void setEpsEntity(EpsEntity epsEntity) {
        this.epsEntity = epsEntity;
    }
}