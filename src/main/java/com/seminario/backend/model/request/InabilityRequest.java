package com.seminario.backend.model.request;

public class InabilityRequest {

    private Long incapId;
    private String tipo;
    private String descripcion;
    private Integer auxilio;

    public InabilityRequest() {
        // Constructor
    }

    public Long getIncapId() {
        return incapId;
    }

    public void setIncapId(Long incapId) {
        this.incapId = incapId;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getAuxilio() {
        return auxilio;
    }

    public void setAuxilio(Integer auxilio) {
        this.auxilio = auxilio;
    }
}