package com.seminario.backend.model.request;

import com.seminario.backend.model.EpsEntity;

public class CompanyRequest {

    private Long empId;
    private String nombre;
    private String direccion;
    private EpsEntity epsEntity;

    public CompanyRequest() {
        // Constructor
    }

    public Long getEmpId() {
        return empId;
    }

    public void setEmpId(Long empId) {
        this.empId = empId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public EpsEntity getEpsEntity() {
        return epsEntity;
    }

    public void setEpsEntity(EpsEntity epsEntity) {
        this.epsEntity = epsEntity;
    }
}