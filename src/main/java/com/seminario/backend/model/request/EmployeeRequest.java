package com.seminario.backend.model.request;

import com.seminario.backend.model.Company;
import com.seminario.backend.model.EpsEntity;
import com.seminario.backend.model.Inability;

public class EmployeeRequest {

    private Long tbId;
    private String nombre;
    private String apellido;
    private Integer edad;
    private Integer salario;
    private EpsEntity epsEntity;
    private Inability inability;
    private Company company;

    public EmployeeRequest() {
        // Constructor
    }

    public Long getTbId() {
        return tbId;
    }

    public void setTbId(Long tbId) {
        this.tbId = tbId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public Integer getSalario() {
        return salario;
    }

    public void setSalario(Integer salario) {
        this.salario = salario;
    }

    public EpsEntity getEpsEntity() {
        return epsEntity;
    }

    public void setEpsEntity(EpsEntity epsEntity) {
        this.epsEntity = epsEntity;
    }

    public Inability getInability() {
        return inability;
    }

    public void setInability(Inability inability) {
        this.inability = inability;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}