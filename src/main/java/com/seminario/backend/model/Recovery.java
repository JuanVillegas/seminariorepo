package com.seminario.backend.model;

import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;

@Entity
@Table(name = "recobro")
public class Recovery {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "reco_id")
    private Long recoId;

    @Column(name = "reco_descripcion")
    private String descripcion;

    @OneToOne
    @JoinColumn(name = "fk_incapacidad_id", referencedColumnName = "incap_id")
    private Inability inability;

    @OneToOne
    @JoinColumn(name = "fk_trabajador_id", referencedColumnName = "tb_id")
    private Employee employee;

    @OneToOne
    @JoinColumn(name = "fk_entidad_id", referencedColumnName = "ent_id")
    private EpsEntity epsEntity;

    public Recovery() {
        // Constructor
    }

    public Long getRecoId() {
        return recoId;
    }

    public void setRecoId(Long recoId) {
        this.recoId = recoId;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Inability getInability() {
        return inability;
    }

    public void setInability(Inability inability) {
        this.inability = inability;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public EpsEntity getEpsEntity() {
        return epsEntity;
    }

    public void setEpsEntity(EpsEntity epsEntity) {
        this.epsEntity = epsEntity;
    }
}