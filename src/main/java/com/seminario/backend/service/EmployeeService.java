package com.seminario.backend.service;

import com.seminario.backend.model.Company;
import com.seminario.backend.model.Employee;
import com.seminario.backend.model.EpsEntity;
import com.seminario.backend.model.Inability;
import com.seminario.backend.repository.CompanyRepository;
import com.seminario.backend.repository.EmployeeRepository;
import com.seminario.backend.repository.EpsEntityRepository;
import com.seminario.backend.repository.InabilityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private EpsEntityRepository epsEntityRepository;

    @Autowired
    private InabilityRepository inabilityRepository;

    @Autowired
    private CompanyRepository companyRepository;

    public Employee saveEmployee(Employee employee){

        EpsEntity epsEntity = epsEntityRepository.getByEntId(employee.getEpsEntity().getEntId());
        employee.setEpsEntity(epsEntity);

        Inability inability = inabilityRepository.getByIncapId(employee.getInability().getIncapId());
        employee.setInability(inability);

        Company company = companyRepository.getByEmpId(employee.getCompany().getEmpId());
        employee.setCompany(company);

        return employeeRepository.save(employee);
    }

    public Employee updateEmployee(Employee employee){
        return employeeRepository.save(employee);
    }

    public List<Employee> findAllEmployee(){
        return employeeRepository.findAll();
    }

    public void deleteEmployee(Long id){
        employeeRepository.deleteById(id);
    }

    public Employee findByEpsEntity(Long id) {
        return employeeRepository.findByEpsEntity(id);
    }

    public Employee findByEmployee(Long id){
        return employeeRepository.findByEmployee(id);
    }
}
