package com.seminario.backend.service;

import com.seminario.backend.model.Inability;
import com.seminario.backend.repository.InabilityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InabilityService {

    @Autowired
    private InabilityRepository inabilityRepository;

    public Inability saveInability(Inability inability){
        return  inabilityRepository.save(inability);
    }

    public Inability updateInability(Inability inability){
        return  inabilityRepository.save(inability);
    }

    public List<Inability> findAllInability(){
        return inabilityRepository.findAll();
    }

    public void deleteInability(Long id){
        inabilityRepository.deleteById(id);
    }
}
