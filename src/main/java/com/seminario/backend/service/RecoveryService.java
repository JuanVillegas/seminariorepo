package com.seminario.backend.service;

import com.seminario.backend.model.Employee;
import com.seminario.backend.model.EpsEntity;
import com.seminario.backend.model.Inability;
import com.seminario.backend.model.Recovery;
import com.seminario.backend.repository.EmployeeRepository;
import com.seminario.backend.repository.EpsEntityRepository;
import com.seminario.backend.repository.InabilityRepository;
import com.seminario.backend.repository.RecoveryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecoveryService {

    @Autowired
    private RecoveryRepository recoveryRepository;

    @Autowired
    private InabilityRepository inabilityRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private EpsEntityRepository epsEntityRepository;

    public Recovery saveRecovery(Recovery recovery){
        Inability inability = inabilityRepository.getByIncapId(recovery.getInability().getIncapId());
        recovery.setInability(inability);

        Employee employee = employeeRepository.getByTbId(recovery.getEmployee().getTbId());
        recovery.setEmployee(employee);

        EpsEntity epsEntity = epsEntityRepository.getByEntId(recovery.getEpsEntity().getEntId());
        recovery.setEpsEntity(epsEntity);

        return recoveryRepository.save(recovery);
    }

    public Recovery updateRecovery(Recovery recovery){
        return recoveryRepository.save(recovery);
    }

    public List<Recovery> findAllRecovery(){
        return recoveryRepository.findAll();
    }

    public void deleteRecovery(Long id){
        recoveryRepository.deleteById(id);
    }

    public Recovery findByRecoveryEmplo(Long id){
        return recoveryRepository.findByRecoveryEmplo(id);
    }

    public Recovery findByRecoveryEntity(Long id){
        return recoveryRepository.findByRecoveryEntity(id);
    }

    public Recovery findByRecoveryInability(Long id){
        return recoveryRepository.findByRecoveryInability(id);
    }
}
