package com.seminario.backend.service;

import com.seminario.backend.model.EpsEntity;
import com.seminario.backend.repository.EpsEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EpsEntityService {

    @Autowired
    private EpsEntityRepository epsEntityRepository;

    public EpsEntity saveEpsEntity(EpsEntity epsEntity){
        return epsEntityRepository.save(epsEntity);
    }

    public EpsEntity updateEpsEntity(EpsEntity epsEntity){
        return epsEntityRepository.save(epsEntity);
    }

    public List<EpsEntity> findAllEpsEntity(){
        return epsEntityRepository.findAll();
    }

    public void deleteEpsEntity(Long id){
        epsEntityRepository.deleteById(id);
    }
}
