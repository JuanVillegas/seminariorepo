package com.seminario.backend.service;

import com.seminario.backend.model.Company;
import com.seminario.backend.model.EpsEntity;
import com.seminario.backend.repository.CompanyRepository;
import com.seminario.backend.repository.EpsEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private EpsEntityRepository epsEntityRepository;

    public Company saveCompany(Company company){
        EpsEntity epsEntity = epsEntityRepository.getByEntId(company.getEpsEntity().getEntId());
        company.setEpsEntity(epsEntity);

        return companyRepository.save(company);
    }

    public Company updateCompany(Company company){
        return companyRepository.save(company);
    }

    public List<Company> findAllCompany(){
        return companyRepository.findAll();
    }

    public void deleteCompany(Long id){
        companyRepository.deleteById(id);
    }
}
