CREATE TABLE "trabajador"(
    tb_id serial NOT NULL,
    tb_nombre character varying(255) NOT NULL,
    tb_apellido character varying(255) NOT NULL,
    tb_edad INTEGER,
    tb_salario INTEGER,
    fk_entidad_id SERIAL NOT NULL,
    fk_incapacidad_id serial NOT NULL,
    fk_empresa_id serial NOT NULL,
    PRIMARY KEY (tb_id));

    ALTER TABLE "trabajador" OWNER to "semibd";

CREATE TABLE "empresa"(
    emp_id serial NOT NULL,
    emp_nombre character varying(255) NOT NULL,
    emp_direccion character varying(255) NOT NULL,
    fk_entidad_id serial NOT NULL,
    PRIMARY KEY (emp_id));

    ALTER TABLE "empresa" OWNER to "semibd";


CREATE TABLE "entidad"(
    ent_id serial NOT NULL,
    ent_nombre character varying(255) NOT NULL,
    ent_descripcion character varying(255) NOT NULL,
    ent_pago INTEGER,
    PRIMARY KEY (ent_id));

    ALTER TABLE "entidad" OWNER to "semibd";


CREATE TABLE "incapacidad"(
    incap_id serial NOT NULL,
    incap_tipo character varying(255) NOT NULL,
    incap_descripcion character varying(255) NOT NULL,
    incap_auxilio INTEGER NOT NULL,
    PRIMARY KEY (incap_id));

    ALTER TABLE "incapacidad" OWNER to "semibd";


CREATE TABLE "recobro"(
    reco_id serial NOT NULL,
    reco_descripcion character varying (255) NOT NULL,
    fk_incapacidad_id serial NOT NULL,
    fk_trabajador_id serial NOT NULL,
    fk_entidad_id serial NOT NULL,
    PRIMARY KEY (reco_id));

    ALTER TABLE "recobro" OWNER to "semibd";


ALTER TABLE "trabajador" ADD CONSTRAINT fk_incapacidad_id FOREIGN KEY (fk_incapacidad_id) REFERENCES "incapacidad" (incap_id);
ALTER TABLE "trabajador" ADD CONSTRAINT fk_empresa_id FOREIGN KEY (fk_empresa_id) REFERENCES "empresa" (emp_id);
ALTER TABLE "trabajador" ADD CONSTRAINT fk_entidad_id FOREIGN KEY (fk_entidad_id) REFERENCES "entidad" (ent_id);
ALTER TABLE "empresa" ADD CONSTRAINT fk_entidad_id FOREIGN KEY (fk_entidad_id) REFERENCES "entidad" (ent_id);
ALTER TABLE "recobro" ADD CONSTRAINT fk_incapacidad_id FOREIGN KEY (fk_incapacidad_id) REFERENCES "incapacidad" (incap_id);
ALTER TABLE "recobro" ADD CONSTRAINT fk_trabajador_id FOREIGN KEY (fk_trabajador_id) REFERENCES "trabajador" (tb_id);
ALTER TABLE "recobro" ADD CONSTRAINT fk_entidad_id FOREIGN KEY (fk_entidad_id) REFERENCES "entidad" (ent_id);